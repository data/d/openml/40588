# OpenML dataset: birds

https://www.openml.org/d/40588

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multi-label dataset. The birds dataset consists of 327 audio recordings of 12 different vocalizing bird species. Each sound can be assigned to various bird species.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40588) of an [OpenML dataset](https://www.openml.org/d/40588). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40588/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40588/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40588/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

